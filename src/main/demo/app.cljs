(ns demo.app
  (:require [shadow.markup.react :as html :refer ($ defstyled)]
            [reagent.core :as r]
            [shadow.dom :as dom]
            ["react" :as react :refer [createElement]]
            ["react-dom" :as react-dom]
            ;; don't need the :refer or :rename
            ;; could just use :as router and then [:> router/Link ...]
            ;; just wanted to match the JS example
            ["react-router-dom" :refer (Route Link) :rename {BrowserRouter Router}]
            ["react-dom/server" :as ReactDOMServer]
            [goog.reflect]))

(enable-console-print!)

;; (defn exported [props]
;;   [:div "Hi, " (:name props)])

;; (def react-comp (r/reactify-component exported))

;; (defn could-be-jsx []
;;   (r/create-element react-comp #js{:name "world"}))

(defn index []
  [:h2 "Home"])

(defn users []
  [:h2 "Users"])

(defn about []
  [:h2 "About"])

;; react-router wants react component classes
(def Index (r/reactify-component index))
(def Users (r/reactify-component users))
(def About (r/reactify-component about))

(defn root []
  [:> Router
   [:div
    [:nav
     [:> Route {:path "/" :exact true :component Index}]
     [:> Route {:path "/about/" :component About}]
     [:> Route {:path "/users/" :component Users}]
     [:ul
      [:li
       [:> Link {:to "/"} "Home"]]
      [:li
       [:> Link {:to "/about/"} "About"]]
      [:li
       [:> Link {:to "/users/"} "Users"]]]]
    ]])

(defn ^:dev/after-load start []
  (js/console.log "app start!")
  ;; (js/console.log (ReactDOMServer/renderToString (createElement "div" nil "Hello World!")))
  ;; (react-dom/render (react/createElement "div" nil "Hello from react!")
  ;;                   (dom/by-id "app"))
  ;; (js/console.log (goog.object/get (js-obj "hello" "OK!") "hello"))
  ;; (r/render [could-be-jsx] (dom/by-id "app"))
  (r/render [root] (dom/by-id "app")))

(defn ^:export init []
  (js/console.log "app init!!!")
  (start))

(defn stop []
  (js/console.log "app stop"))
